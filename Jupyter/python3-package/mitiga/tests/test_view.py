import unittest
from mitiga import View, Utilities


class TestView(unittest.TestCase):
    def test_V_RISCHIO_P_A_H_D(self):
        self.assertIsNot(None, View().V_RISCHIO_P_A_H_D)

    def test_V_RISCHIO_P_A(self):
        self.assertIsNotNone(View().V_RISCHIO_P_A)

    def test_V_EFFICACIA_A_H(self):
        self.assertIsNotNone(View().V_EFFICACIA_A_H)

    def test_V_MITIG_AND_APPL_A_H_C(self):
        self.assertIsNotNone(View().V_MITIG_AND_APPL_A_H_C)

    def test_V_RISCHIO_P_A_GROUPBY_ASSESSMENTID(self):
        self.assertIsNotNone(View().V_RISCHIO_P_A_GROUPBY_ASSESSMENTID)

    def test_V_RISCHIO_P_A_QUERY_ASSESSMENTID_LIST_ONE(self):
        self.assertIsNotNone(View().V_RISCHIO_P_A_QUERY_ASSESSMENTID(['0e6381be-83f7-4451-9ec7-117efecb4a56']))

    def test_V_RISCHIO_P_A_QUERY_ASSESSMENTID_LIST_LIST(self):
        column_names, list_by_row = View().V_RISCHIO_P_A_GROUPBY_ASSESSMENTID
        aid_list, count_list = Utilities.switch_rows_and_columns(list_by_row)
        del count_list

        result = View().V_RISCHIO_P_A_QUERY_ASSESSMENTID(aid_list)
        self.assertIsNotNone(result)


if __name__ == '__main__':
    unittest.main()
