import os
import glob
import unittest
from .test_config_manager import TestConfigManager
from .test_db_connector import TestDbConnector
from .test_displayer import TestDisplayer
from .test_utilities import TestUtilities
from .test_view import TestView
from .test_formulae import TestFormulae

__all__ = ['TestConfigManager',
           'TestDbConnector', 'TestDisplayer',
           'TestUtilities', 'TestView', 'TestFormulae']

def load_tests(loader, tests, pattern):
    # suite = unittest.TestSuite()
    # suite.addTests(loader.loadTestsFromModule(foo))
    # suite.addTests(loader.loadTestsFromModule(bar))

    loader = unittest.TestLoader()
    start_dir = os.path.dirname(__file__)
    suite = loader.discover(start_dir)
    # runner = unittest.TextTestRunner()
    # runner.run(suite)

    return suite
