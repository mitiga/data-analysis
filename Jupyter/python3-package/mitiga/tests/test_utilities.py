import unittest
from mitiga import Utilities, View


class TestUtilities(unittest.TestCase):
    def test_switch_rows_and_columns(self):
        column_names, list_by_row = View().V_RISCHIO_P_A_GROUPBY_ASSESSMENTID
        list_by_columns = Utilities.switch_rows_and_columns(list_by_row)
        # self.assertCountEqual()
        self.assertEqual(len(column_names), len(list_by_columns))

    def test_get_res_risk_avg_assessmentid(self):
        res = Utilities.get_res_risk_avg_assessmentid('0e6381be-83f7-4451-9ec7-117efecb4a56')
        self.assertIsNotNone(res)
        self.assertEqual(2335611.7835481316, res)
        # tmp average associated with this assessmentid

    def test_get_res_risk_avg_foreach_assessmentid(self):
        col_names, res = Utilities.get_res_risk_avg_foreach_assessmentid()

        self.assertIsNotNone(col_names)
        self.assertEqual(len(col_names), 3)
        self.assertIsNotNone(res)

    def test_get_res_risk_variance_assessmentid(self):
        count, med, med_bt, var, ci =\
            Utilities.get_res_risk_variance_assessmentid('0e6381be-83f7-4451-9ec7-117efecb4a56')

        self.assertIsNotNone(count)
        self.assertIsNotNone(med)
        self.assertIsNotNone(med_bt)
        self.assertIsNotNone(var)
        self.assertIsNotNone(ci)

    def test_get_res_risk_variance_foreach_assessmentid(self):
        col_names, res = Utilities.get_res_risk_variance_foreach_assessmentid(size=100)

        self.assertIsNotNone(col_names)
        self.assertEqual(len(col_names), 6)
        self.assertIsNotNone(res)


if __name__ == '__main__':
    unittest.main()
