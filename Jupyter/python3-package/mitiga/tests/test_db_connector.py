import unittest
from mitiga.db_connector import DbConnector


class TestDbConnector(unittest.TestCase):
    def test_connection_fails(self):
        dbc = DbConnector(username='username', password='password')

        self.assertEqual(False, dbc.is_connected)

    def test_connection_succeed(self):
        dbc = DbConnector()
        self.assertEqual(True, dbc.is_connected)

    def test_execute_query_and_convert_no_query(self):
        dbc = DbConnector()
        self.assertRaises(TypeError, dbc.execute_query_and_convert)

    def test_execute_query_and_convert_not_wf_query(self):
        dbc = DbConnector()
        query = F'SELECT DBTIMEZONE FROM PUBLIC'
        self.assertRaises(ConnectionError, dbc.execute_query_and_convert, query)

    def test_execute_query_and_convert_wf_query(self):
        dbc = DbConnector()
        query = F'SELECT DBTIMEZONE FROM DUAL'
        table, value = dbc.execute_query_and_convert(query)
        self.assertEqual('DBTIMEZONE', table[0])
        self.assertEqual('+00:00', value[0][0])


if __name__ == '__main__':
    unittest.main()
