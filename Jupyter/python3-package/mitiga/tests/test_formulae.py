import unittest
from mitiga import Formulae


class TestFormulae(unittest.TestCase):
    def test_bootstrap_median_variance(self):
        values = [3, 7, 2, 9, 5, 1, 8, 4, 6]

        count, med, med_bt, var, ci = Formulae.bootstrap_median_variance(values)

        self.assertEqual(len(values), count)
        self.assertEqual(med, 5)
        self.assertIsNotNone(med_bt)
        self.assertIsNotNone(var)
        self.assertIsNotNone(ci)

    def test_average(self):
        values = [3, 7, 2, 9, 5, 1, 8, 4, 6]

        count, avg = Formulae.average(values)

        self.assertEqual(len(values), count)
        self.assertEqual(avg, 5.)

    def test_median(self):
        values = [3, 7, 2, 9, 5, 1, 8, 4, 6]

        count, med = Formulae.median(values)

        self.assertEqual(len(values), count)
        self.assertEqual(med, 5.)


if __name__ == '__main__':
    unittest.main()
