import unittest
from mitiga.config.config_manager import ConfigManager


class TestConfigManager(unittest.TestCase):
    def test_get_config_not_None(self):
        self.assertIsNotNone(ConfigManager.get_config())

    def test_search_config_is_None(self):
        self.assertIsNone(ConfigManager.search_config())

    def test_search_config_str_not_existing(self):
        self.assertIsNone(ConfigManager.search_config('not_existing_param'))

    def test_search_config_str_exists(self):
        self.assertIsNotNone(ConfigManager.search_config('db'))

    def test_search_config_list_empty(self):
        self.assertIsNone(ConfigManager.search_config([]))

    def test_search_config_list_not_existing(self):
        self.assertIsNone(ConfigManager.search_config(['db', 'not_existing_db']))

    def test_search_config_list_exists(self):
        self.assertEqual(ConfigManager.search_config(['db', 'oracle', 'default', 'username']), 'MTG04')


if __name__ == '__main__':
    unittest.main()
