# -*- coding: utf-8 -*-
import json
import os


class ConfigManager(object):
    """
    Static class to access the configurations.
    """
    @staticmethod
    def search_config(param=None):
        """
        Returns the required configuration one was searching for
        :param param: string or [string], the list of parameters to make the search
        :return: the required configuration, with the same type of it, or None.
        """
        try:
            if not param:
                return None

            config = ConfigManager.__open_file()

            if type(param) is str:
                return config[param]

            if type(param) is list:
                res = config
                for p in param:
                    res = res[str(p)]
                return res

            return None
        except:
            return None

    @staticmethod
    def get_config():
        """

        :return:
        """
        return ConfigManager.__open_file()

    @staticmethod
    def __open_file():
        """
        Deserializes the JSON document's content

        :return: The content of the JSON file, mapped on Python's structures.
        """
        # with open('/dev.json', 'r') as f:
        with open(os.path.join(os.path.dirname(__file__), 'dev.json'), 'r') as f:
                return json.load(f)
