# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from IPython.display import HTML, display


class Displayer(object):
    @staticmethod
    def html_table(data: (list, list(tuple()))):
        """
        Represents a constructor for a a html table, to show in a organized table.

        :param data: a table structure, organized in row visualization.
        """
        table = HTML(
            '<table>{}</table>'
                .format('<tr>{}</tr>{}'
                        .format(''
                                .join(('<th>{}</th>'
                                       .format(col_head)) for col_head in data[0]),
                                '{}'
                                .format(''
                                        .join('<tr>{}</tr>'
                                              .format(''
                                                      .join('<td>{}</td>'
                                                            .format(col)
                                                            for col in row)
                                                      )
                                              for row in data[1])
                                        )
                                )
                        )
        )

        display(table)

    @staticmethod
    def show_histogram(values: list, title: str = None, x_label: str = None, y_label: str = None, x_ticks: list = None):
        """
        Represents a constructor for a histogram, to show data visually.

        :param values: the values that one must represents in the histogram
        :param title: the title of the histogram
        :param x_label: the label of the x-axes
        :param y_label: the label of the y-axes
        :param x_ticks: the name associated with every tick(that is, the label for every single value)
        """
        if title is not None:
            plt.title(title)
        if x_label is not None:
            plt.xlabel = x_label
        if y_label is not None:
            plt.ylabel = y_label
        if x_ticks is not None:
            plt.xticks(range(len(x_ticks)), x_ticks)

        plt.grid(True)
        plt.bar(range(len(values)), values, align='center')
        plt.show()
