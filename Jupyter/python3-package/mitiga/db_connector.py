# -*- coding: utf-8 -*-
import cx_Oracle
from .config import ConfigManager


class DbConnector(object):
    """This class represent the connection within the Oracle DB.

    If the class has public attributes, they may be documented here
    in an ``Attributes`` section and follow the same formatting as a
    function's ``Args`` section. Alternatively, attributes may be documented
    inline with the attribute's declaration (see __init__ method below).

    Properties created with the ``@property`` decorator should be documented
    in the property's getter method.
    """

    def __init__(self, username=None, password=None, hostname=None):
        """The constructor of the class.

                It is responsible of initializing all the properties of the class

                Attributes
                ----------
                username : str, optional
                    the username used to connect with the db.
                password : float, optional
                    the password used to connect with the db
                hostname : float, optional
                    the hostname of the db one is connecting to.

                Examples
                --------
                dbc = DbConnector("user", "pass", "localhost/oracle_service")
        """
        default = ConfigManager.search_config(['db', 'oracle', 'default'])
        if not username:
            username = default['username']
        if not password:
            password = default['password']
        if not hostname:
            hostname = default['hostname']

        self.__username = username
        self.__password = password
        self.__hostname = hostname
        self.__connection = None

    @property
    def __connect(self) -> cx_Oracle.Connection:
        """
        Private readonly property that represents the connection with the oracle db.

        :return: a Connection element to communicate with the db.
        """
        try:
            if not self.__connection:
                # Connect as user "__user_name" with password "__password" to the  service running on "__host_name".
                self.__connection = cx_Oracle.connect(self.__username, self.__password, self.__hostname)

            return self.__connection
        except:
            raise ConnectionError

    def __execute_query(self, query: str) -> cx_Oracle.Cursor:
        """
        Executes a query on the db.

        :param query: the query that has to be executed on the db.
        :return: the cursor with the information returned after the query.
        """
        return self.__connect.cursor().execute(query)

    @staticmethod
    def __convert_cursor_to_tuple(cursor: cx_Oracle.Cursor) -> (list, list(tuple())):
        """
        Converts the information in a cursor in a tuple made of two lists.

        :param cursor: the cursor used to make a query.
        :return: the information in the cursor organized in a tuple with 2 list, that is: (names, results).
        """
        ret_list = cursor.fetchall()
        ret_names = []
        for column in cursor.description:
            ret_names.append(column[0])

        return ret_names, ret_list

    @property
    def is_connected(self) -> bool:
        """
        Gets if the connection with the db is up and running.
        :return: True if connected, False otherwise
        """
        try:
            if self.__connect:
                return True
            return False
        except ConnectionError:
            return False

    def execute_query_and_convert(self, query: str) -> (list, list(tuple())):
        """
        Executes a query on the db and organizes the information in a tuple made of two lists.

        :param query: the query that has to be executed on the db.
        :return: the information in the cursor organized in a tuple with 2 list, that is: (names, results).
        """
        try:
            cursor = self.__execute_query(query)
            ret_values = self.__convert_cursor_to_tuple(cursor)
            self.__connect.close()
            self.__connection = None
            return ret_values
        except cx_Oracle.DatabaseError:
            raise ConnectionError
