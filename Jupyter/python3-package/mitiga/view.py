# -*- coding: utf-8 -*-
from . import DbConnector


class View(object):
    """Represents an utility class to easily query the data source provided.

        Examples
        --------
        view = View(dbc)

    """

    def __init__(self, db_connector: DbConnector = None):
        if not db_connector:
            db_connector = DbConnector()
        self.__dbc = db_connector

    @property
    def V_RISCHIO_P_A_H_D(self) -> (list, list(tuple())):
        query = F"SELECT * FROM V_RISCHIO_P_A_H_D"

        return self.__dbc.execute_query_and_convert(query)

    @property
    def V_RISCHIO_P_A(self) -> (list, list(tuple())):
        query = F"SELECT * FROM V_RISCHIO_P_A"

        return self.__dbc.execute_query_and_convert(query)

    @property
    def V_EFFICACIA_A_H(self) -> (list, list(tuple())):
        query = F"SELECT * FROM V_EFFICACIA_A_H"

        return self.__dbc.execute_query_and_convert(query)

    @property
    def V_MITIG_AND_APPL_A_H_C(self) -> (list, list(tuple())):
        query = F"SELECT * FROM V_MITIG_AND_APPL_A_H_C"

        return self.__dbc.execute_query_and_convert(query)

    @property
    def V_RISCHIO_P_A_GROUPBY_ASSESSMENTID(self) -> (list, list(tuple())):
        query = F"SELECT assessmentid, COUNT(*) FROM V_RISCHIO_P_A GROUP BY assessmentid ORDER BY assessmentid ASC"

        return self.__dbc.execute_query_and_convert(query)

    def V_RISCHIO_P_A_QUERY_ASSESSMENTID(self, aid_list: (str)) -> (list, list(tuple())):
        q_select = "SELECT assessmentid, res_risk"
        q_from = "FROM V_RISCHIO_P_A"
        q_where_start = "WHERE"
        q_where_end = F"assessmentid = '{aid_list[-1]}'"
        q_where_iterator = [F" assessmentid = '{aid}' OR" for aid in aid_list if aid is not aid_list[-1]]
        q_order_by = F"ORDER BY assessmentid ASC"
        query = F"{q_select} {q_from} {q_where_start}{F''.join(q_where_iterator)} {q_where_end} {q_order_by}"

        return self.__dbc.execute_query_and_convert(F"{query}")
