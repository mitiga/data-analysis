# -*- coding: utf-8 -*-
import random
import numpy as np
import scipy.stats as sps
import bootstrapped.bootstrap as bs
import bootstrapped.stats_functions as bs_stats


class Formulae(object):
    @staticmethod
    def bootstrap_median_variance(values: list, size: int = 10000) -> (int, float, list, float, float):
        """
        Gets the average of the values in the provided list
        :type values: list
        :param values: a list of elements
        :type size: int
        :param size: the number of repetitions
        :return: a tuple with:
            the number of elements processed,
            the median,
            the BT sample medians,
            the variance,
            the confidence interval
            (count, med, med_bt, var, ci)
        """
        # var = bs.bootstrap(values=np.array(values), stat_func=bs_stats.sum)

        count, med = Formulae.median(values)

        # med_bt = np.repeat(None, size)
        med_bt = list()
        # med_bt.append(np.median(x_bt))

        # med_bt = [np.median([values[i] for i in np.random.choice(n, n, True, None)]) for _ in np.repeat(None, size)]

        n = count
        tmp_list = list()
        for curr in range(size):
            # sampling with replacement
            w = np.random.choice(n, n, True, None)
            x_bt = [values[i] for i in w]
            med_bt.append(np.median(x_bt))

        var = np.var(med_bt)
        # confidence interval
        # ci = np.quantile(0.95) * np.std(med_bt)
        ci = sps.norm.ppf(0.95) * np.std(med_bt)
        # ci = [med + ci, med - ci]

        return count, med, med_bt, var, ci

    @staticmethod
    def average(values: list) -> (int, float):
        """
        Gets the average of the values in the provided list
        :type values: list
        :param values: a list of elements
        :return: a tuple with the amount of elements processed and the avg (count, avg)
        """
        count = len(values)
        avg = np.average(values)

        return count, avg

    @staticmethod
    def median(values: list) -> (int, float):
        """
        Gets the median of the values in the provided list
        :type values: list
        :param values: a list of elements
        :return: a tuple with the amount of elements processed and the median (count, med)
        """
        count = len(values)
        med = np.median(values)

        return count, med
