# -*- coding: utf-8 -*-
import random


from .py_linq import Enumerable
from . import View
from . import Formulae
import numpy as np
import bootstrapped.bootstrap as bs
import bootstrapped.stats_functions as bs_stats


class Utilities(object):
    @staticmethod
    def switch_rows_and_columns(data: list(tuple())) -> list(list()):
        """
        Switches row/column visualization in column/row
        :param data: the columns/rows that must be converted in rows/columns
        :return: the data provided, with the switched POV
        """
        result_list = list()
        row_length = len(data[0])  # n_columns

        for i in range(row_length):
            result_list.append(list())

        for row in data:
            for j, value in enumerate(row):
                result_list[j].append(value)

        return result_list

    @staticmethod
    def get_res_risk_avg_assessmentid(aid: str, view: View = None) -> float:
        """

        :type aid: str
        :param aid: the assessment id you want to calculate the avg for
        :type view: View
        :param view: the view to query, if any
        :return: the value of the average of the elements in the list
        """
        if not view:
            view = View()
        column_names, list_by_row = view.V_RISCHIO_P_A_QUERY_ASSESSMENTID([aid])
        aid_list, values = Utilities.switch_rows_and_columns(list_by_row)
        count, avg = Formulae.average(values)

        return avg

    @staticmethod
    def get_res_risk_avg_foreach_assessmentid(view: View = None) -> (list, list):
        """
        Gets the avg risk associated with every assessment id
        :type view: View
        :param view: the view that one wants to query, if any
        :return: a list containing two lists, the first with the columns names and the second with the results
        """
        if not view:
            view = View()

        column_names, list_by_row = view.V_RISCHIO_P_A_GROUPBY_ASSESSMENTID
        aid_list, count_list = Utilities.switch_rows_and_columns(list_by_row)
        column_names, res_list_all = view.V_RISCHIO_P_A_QUERY_ASSESSMENTID(aid_list)

        del column_names

        res_list = []

        coll = Enumerable(res_list_all).group_by(key_names=['id'], key=lambda x: x[0])

        for group in coll:
            group_id, count, avg = Utilities.__get_res_risk_avg_group(group)

            res_list.append((group_id, count, avg))

        return (('ASSESSMENTID', 'COUNT', 'RES_RISK'), res_list)

    @staticmethod
    def get_res_risk_variance_assessmentid(aid: str, view: View = None) -> (int, float, list, float, float):
        """

        :type aid: str
        :param aid: the assessment id you want to calculate the avg for
        :type view: View
        :param view: the view to query, if any
        :return: the value of the average of the elements in the list
        """
        if not view:
            view = View()
        column_names, list_by_row = view.V_RISCHIO_P_A_QUERY_ASSESSMENTID([aid])
        aid_list, values = Utilities.switch_rows_and_columns(list_by_row)
        count, med, med_bt, var, ci = Formulae.bootstrap_median_variance(values)

        return count, med, med_bt, var, ci

    @staticmethod
    def get_res_risk_variance_foreach_assessmentid(view: View = None, size: int = 10000) -> (list, list):
        """
        Gets the avg risk associated with every assessment id
        :type view: View
        :param view: the view that one wants to query, if any
        :type size: int
        :param size: the number of repetitions to calculate the variance
        :return: a list containing two lists, the first with the columns names and the second with the results
        """
        if not view:
            view = View()

        column_names, list_by_row = view.V_RISCHIO_P_A_GROUPBY_ASSESSMENTID
        aid_list, count_list = Utilities.switch_rows_and_columns(list_by_row)
        column_names, res_list_all = view.V_RISCHIO_P_A_QUERY_ASSESSMENTID(aid_list)

        del column_names

        res_list = []

        coll = Enumerable(res_list_all).group_by(key_names=['id'], key=lambda x: x[0])

        for group in coll:
            group_id, count, med, med_bt, var, ci = Utilities.__get_bootstrap_median_variance_group(group, size)
            res_list.append((group_id, count, med, med_bt, var, ci))
        vvv = 12
        return (('ASSESSMENTID', 'COUNT', 'MEDIAN','MEDIAN_BT','VARIANCE', 'CONF_INTERVAL'), res_list)

    @staticmethod
    def __get_res_risk_avg_group(group: Enumerable) -> (str, int, float):
        """
        Gets the average of the values in the provided group
        :type group: Enumerable3
        :param group: a Grouping/3 element (see py_linq documentation)
        :return: a tuple with the id, the amount of elements processed and the avg (group_id, count, avg)
        """
        group_id = group.key.id
        count = group.count()
        values = [value for key, value in group]
        count, avg = Formulae.average(values)
        # var = bs.bootstrap(values=np.array(values), stat_func=bs_stats.sum)

        return group_id, count, avg

    @staticmethod
    def __get_bootstrap_median_variance_group(group: Enumerable, size: int = 10000) -> (str, int, float, list, float, float):
        """
        Gets the average of the values in the provided group
        :type group: Enumerable3
        :param group: a Grouping/3 element (see py_linq documentation)
        :type size: int
        :param size: the number of repetitions to calculate the variance
        :return: a tuple with the id, the amount of elements processed and the avg (group_id, count, avg)
        """
        group_id = group.key.id
        values = [value for key, value in group]
        count, med, med_bt, var, ci = Formulae.bootstrap_median_variance(values, size)
        # var = bs.bootstrap(values=np.array(values), stat_func=bs_stats.sum)

        return group_id, count, med, med_bt, var, ci
