# -*- coding: utf-8 -*-
from setuptools import setup

setup(
      name='mitiga',
      version='0.3',
      description='scientific library to interact and visualize data from the mitiga environment',
      url='http://www.mitiga.it',
      author='Davide Quattrocchi',
      author_email='davide.quattrocchi@senseisrl.it',
      # license='MIT',
      packages=['mitiga', 'mitiga.config', 'mitiga.tests', 'mitiga.py_linq'],
      package_data={'': ['*.json']},
      install_requires=[
            'cx_Oracle', 'matplotlib', 'IPython', 'numpy', 'bootstrapped', 'scipy'
      ],
      zip_safe=True,
      test_suite='mitiga.tests'
)
